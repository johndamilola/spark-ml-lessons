import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.rdd.RDD

object WordCount {
    def main(args: Array[String]) {
        val conf = new SparkConf()
            .setAppName("Word Count")
            .setMaster("local[*]")

        val sc = new SparkContext(conf)

        val resourceURL = "src/main/resources/README.md"
        val outputURL = "src/main/resources/RESULT.md"

        val text = sc.textFile(resourceURL)
        println("The number of lines are" + text.count())

        val words = text.flatMap(line => line.split(" "))
        val counts = words.map(word => (word, 1)).reduceByKey{case (x, y) => x + y}

        counts.saveAsTextFile(outputURL)

        sc.stop()
    }
}