import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD

object MusicRecommender {
    def main(args: Array[String]) {
        val conf = new SparkConf()
            .setAppName("Fast.me Music")
            .setMaster("local[*]")

        val sc = new SparkContext(conf)

        val rawUserArtistData = sc.textFile("src/main/resources/dataset/user_artist_data.txt")
        val rawArtistData = sc.textFile("src/main/resources/dataset/artist_data.txt")
        val rawArtistAlias = sc.textFile("src/main/resources/dataset/artist_alias.txt")

        rawUserArtistData.map(_.split(" ")(0).toDouble).stats
        rawUserArtistData.map(_.split(" ")(1).toDouble).stats

        val artistByID = rawArtistData.flatMap { line =>
            val (id, name) = line.span(_ != '\t')
            if (name.isEmpty) {
                None
            } else {
                try {
                    Some((id.toInt, name.trim))
                } catch {
                    case e: NumberFormatException => None
                }
            }
        }

        val artistsAlias = rawArtistAlias.flatMap { line =>
            val tokens = line.split("\t")
            if (tokens(0).isEmpty) {
                None
            } else {
                Some((tokens(0).toInt, tokens(1).toInt))
            }
        }.collectAsMap()

        sc.stop()
    }
}