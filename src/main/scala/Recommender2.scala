import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.recommendation.{ALS, Rating, MatrixFactorizationModel}
import org.apache.spark.rdd._

import org.apache.log4j.Logger
import org.apache.log4j.Level

object Recommender2 {
    def main(args: Array[String]) {
        val conf = new SparkConf()
            .setAppName("movie")
            .setMaster("local[*]")

        val sc = new SparkContext(conf)

        val movieDataset = "src/main/resources/dataset/movies.dat"
        val personalRatingDataset = "src/main/resources/dataset/personalRatings.dat"
        val ratingsDataset = "src/main/resources/dataset/ratings.dat"

        val parseRatings: (String) => Rating = (line: String) => {
            line.split("::").take(3) match {
                case Array(userId, movieId, rating) => Rating(userId.toInt, movieId.toInt, rating.toInt)
            }
        }

        val parseMovie: (String) => (Int, String) =(line: String) => {
            line.split("::").take(2) match {
                case Array(movieId, movieTitle) => (movieId.toInt, movieTitle)
            }
        }

        val movieRDD: RDD[(Int, String)] = sc.textFile(movieDataset).map(parseMovie).persist
        val personalRatingRDD: RDD[Rating] = sc.textFile(personalRatingDataset).map(parseRatings).persist
        val ratingsRDD: RDD[Rating] = sc.textFile(ratingsDataset).map(parseRatings).persist

        val numRatings = ratingsRDD.count()
        val numUsers = ratingsRDD.map(_.user).distinct().count()
        val numMovies = ratingsRDD.map(_.product).distinct().count()

        println("===================================")
        println(s"Got $numRatings ratings from $numUsers users on $numMovies movies")
        println("===================================")

        val (training, validation, test) = ratingsRDD.randomSplit(Array(.6, .2, .2)) match {
            case Array(training, validation, test) => (training.union(personalRatingRDD).persist, validation.persist, test.persist)
        }

        println(s"Training : ${training.count}, validation : ${validation.count}, test : ${test.count}")

        sc.stop()
    }
}