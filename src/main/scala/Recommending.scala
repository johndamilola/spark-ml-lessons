import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Recommending2 extends App {
    val conf = new SparkConf()
        .setAppName("Recommender App")
        .setMaster("local[*]")
    
    val sc = new SparkContext(conf)

    val ratingsRDD = sc.textFile("src/main/resources/dataset/ratings.dat")
    val ratings = ratingsRDD.map(line => line.split("::")(2))

    val result = ratings.countByValue()

    val sortedaResult = result.toSeq.sortBy(_._1)

    sortedaResult.foreach(println)

    sc.stop
}