import org.apache.spark.mllib.linalg._
import org.apache.spark.mllib.stat.Statistics
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object CorrelationApp {
    def main(args: Array[String]) = {
        val conf = new SparkConf()
            .setAppName("correlation")
            .setMaster("local[*]")

        val sc = new SparkContext(conf)
        
        val seriesX: RDD[Double] = sc.parallelize(Array(1,2,3,4,5))

        val seriesY: RDD[Double] = sc.parallelize(Array(11,22,33,44,555))

        val correlation = Statistics.corr(seriesX, seriesY, "pearson")
        println(s"The correlation is $correlation")


        val data: RDD[Vector] = sc.parallelize(
            Seq(
                Vectors.dense(1.0, 10.0, 100.0),
                Vectors.dense(2.0, 20.0, 200.0),
                Vectors.dense(5.0, 33.0, 366.0)
            )
        )

        val correlMatrix: Matrix = Statistics.corr(data, "spearman")
        // println(correlMatrix.toString)

        sc.stop()
    }
}

