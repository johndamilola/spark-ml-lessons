import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.mllib.classification.NaiveBayes
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint

object RoadSafety extends App {
    val conf = new SparkConf()
        .setAppName("Road Safety App")
        .setMaster("local[*]")
    
    val sc = new SparkContext(conf)

    val data = sc.textFile("src/main/resources/dataset/DigitalBreathTestData2013.txt")

    val results = data.map(line => enumerateCsvRecord(line.split(","))) take 100 drop 1

    results.foreach(println)

    val arrayData = data.map { line => 
        val parts = line.split(',')
        LabeledPoint(parts(0).toDouble, Vectors.dense(parts(1).split(' ').map(_.toDouble)))
    }

    // val splitData = arrayData.randomSplit(Array(0.7, 0.3), seed = 13L)
    // val trainDataSet = splitData(0)
    // val testDataSet = splitData(1)

    // val nbTrained = NaiveBayes.train(trainDataSet)
    // val nbPredict = nbTrained.predict(testDataSet.map(_.features))

    // val predictionAndLabel = nbPredict.zip(testDataSet.map(_.label))
    // val accuracy = 100.0 * predictionAndLabel.filter(x => x._1 == x._2).count() / testDataSet.count()
    
    // println( "Accuracy : " + accuracy );

    def enumerateCsvRecord(row: Array[String]): String = {
        val col1 = row(0) match {
            case "Moving Traffic Violation" => 0
            case "Other" => 1
            case "Road Traffic Collision" => 2
            case "Suspicion of Alcohol" => 3
            case _ => 99
        }

        val col2 = row(3) match {
            case "Weekday" => 0
            case "Weekend" => 1
            case _ => 99
        }

        val col3 = row(4) match {
            case "12am-4am" => 0
            case "4am-8am" => 1
            case "8am-12pm" => 2
            case "12pm-4pm" => 3
            case "4pm-8pm" => 4
            case "8pm-12pm" => 5
            case _ => 99
        }

        val col4 = row(5)

        val col5 = row(6) match {
            case "16-19" => 0
            case "20-24" => 1
            case "25-29" => 2
            case "30-39" => 3
            case "40-49" => 4
            case "50-59" => 5
            case "60-69" => 6
            case "70-98" => 7
            case "Other" => 8
            case _ => 99
        }

        val col6 = row(7) match {
            case "Male" => 0
            case "Female" => 1
            case "Unknown" => 3
            case _ => 99
        }

        val lineString = col6 + "," + col1 + "," + col2 + "," + col3 + "," + col4 + "," + col5
        return lineString 
    }

    sc.stop
}