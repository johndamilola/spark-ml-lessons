import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.SparkContext._
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.clustering.{KMeans, KMeansModel}

object RoadSafety2 extends App {
    val conf = new SparkConf()
        .setAppName("Road Safety App")
        .setMaster("local[*]")
    
    val sc = new SparkContext(conf)

    val data = sc.textFile("src/main/resources/dataset/digi2.txt")

    val vectorData = data.map { line =>
        Vectors.dense(line.split(',').map(_.toDouble))
    }

    val kMeans = new KMeans
    val numClusters = 3
    val maxIterations = 50

    val initializationMode = KMeans.K_MEANS_PARALLEL
    val numRuns = 1
    val numEpsilon = 1e-4

    kMeans.setK(numClusters)
    kMeans.setMaxIterations(maxIterations)
    kMeans.setInitializationMode(initializationMode)
    kMeans.setRuns(numRuns)
    kMeans.setEpsilon(numEpsilon)

    vectorData.cache
    val kMeansModel = kMeans.run( vectorData )

    val kMeansCost = kMeansModel.computeCost(vectorData)
    println("Imput data rows: " + vectorData.count() )
    println("K-Means cost: " + kMeansCost )

    kMeansModel.clusterCenters.foreach{ println }

    val clusterRddInt = kMeansModel.predict( vectorData )
    val clusterCount = clusterRddInt.countByValue

    clusterCount.toList.foreach{ println }

    sc.stop()
}